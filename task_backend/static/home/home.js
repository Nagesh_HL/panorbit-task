angular.module('home', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('home').config(function($stateProvider) {

    /* Add New States Above */
    $stateProvider.state('home', {
    	url: '/home',
        templateUrl: 'home/partial/homescreen/dashboard.html',
        controller: 'HomescreenCtrl'
    }).state('home.homescreen', {
    	url: '/homescreen',
        templateUrl: 'home/partial/homescreen/homescreen.html',
        controller: 'HomescreenCtrl'
    });
});

